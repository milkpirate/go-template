PROG_NAME=program_name

BINARY_DARWIN64=bin/$(PROG_NAME)-darwin-amd64

BINARY_WIN32=bin/$(PROG_NAME)-win-i386.exe
BINARY_WIN64=bin/$(PROG_NAME)-win-amd64.exe

BINARY_LINUX32=bin/$(PROG_NAME)-linux-i386
BINARY_LINUX64=bin/$(PROG_NAME)-linux-amd64

CI_COMMIT_REF_NAME ?= $(shell git rev-parse --abbrev-ref HEAD)
VERSION ?= $(shell git describe --tags ||  git rev-list -1 HEAD)
BUILD_DATE = $(shell date)

LDFLAGS="-s -w -X main.RepoBranch=$(CI_COMMIT_REF_NAME) -X main.Version=$(VERSION) -X 'main.BuildDate=$(BUILD_DATE)'"

.PHONY: test build-linux build-win build-mac clean dist-clean

build:	build-mac build-win build-linux

build-mac:
	cd cmd/$(PROG_NAME) && GO111MODULE=on CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 \
	    go build -ldflags $(LDFLAGS) -o ../../$(BINARY_DARWIN64)

build-linux:
	cd cmd/$(PROG_NAME) && GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=386 \
	    go build -ldflags $(LDFLAGS) -o ../../$(BINARY_LINUX32)
	cd cmd/$(PROG_NAME) && GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
	    go build -ldflags $(LDFLAGS) -o ../../$(BINARY_LINUX64)

build-win:
	cd cmd/$(PROG_NAME) && GO111MODULE=on CGO_ENABLED=0 GOOS=windows GOARCH=386 \
	    go build -ldflags $(LDFLAGS) -o ../../$(BINARY_WIN32)
	cd cmd/$(PROG_NAME) && GO111MODULE=on CGO_ENABLED=0 GOOS=windows GOARCH=amd64 \
	    go build -ldflags $(LDFLAGS) -o ../../$(BINARY_WIN64)

lint:
	go fmt -x ./cmd/$(PROG_NAME)

upx: build-win build-linux build-mac
	cd bin/
	upx -f --brute -o $(BINARY_DARWIN64).upx $(BINARY_DARWIN64)
	upx -f --brute -o $(BINARY_WIN32).upx $(BINARY_WIN32)
	upx -f --brute -o $(BINARY_WIN64).upx $(BINARY_WIN64)
	upx -f --brute -o $(BINARY_LINUX32).upx $(BINARY_LINUX32)
	upx -f --brute -o $(BINARY_LINUX64).upx $(BINARY_LINUX64)


test-app:
	go test -v -cover -coverprofile=coverage_app.out ./cmd/$(PROG_NAME)

test-lib:
	go test -v -cover -coverprofile=coverage.out ./pkg

test: test-app test-lib
	grep -v "^mode:" coverage_app.out >> coverage.out
	go tool cover -func=coverage.out

dist-clean: clean
	rm -f *.out \
	    $(BINARY_DARWIN64) \
	    $(BINARY_WIN32) \
	    $(BINARY_WIN64) \
	    $(BINARY_LINUX32) \
	    $(BINARY_LINUX64)

clean:
	cd cmd/$(PROG_NAME) && go clean -r

