module gitlab.com/milkpirate/go-template

go 1.13

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/nikogura/gomason v0.0.0-20190612230005-8216ab211e07 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
)
