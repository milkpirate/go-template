package main

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	app := App{
		logger: log.New(),
		args:   os.Args,
	}

	err := app.Run()
	if err != nil {
		app.logger.Fatal(err) // ~ log; return 1
	}
}
