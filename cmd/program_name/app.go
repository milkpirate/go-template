package main

import (
	"fmt"
	"github.com/docopt/docopt-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/milkpirate/go-template/pkg"
	"path"
)

var (
	// Version dito
	Version string = "manual build"

	// RepoBranch dito
	RepoBranch string = Version

	// BuildDate dito
	BuildDate string = Version
)

const (
	usageTemplate = `
Program:
    Some program description.

Usage:
    %[1]s
    %[1]s -h | --help
    %[1]s -v | --version

Options:
    -v --version        Show version.
    -h --help           Show this screen.

©2018 Paul Schroeder - paul.schroeder@mail.com
`
)

type App struct {
	logger *log.Logger
	args   []string
	opts   docopt.Opts
}

func (app *App) Run() error {
	app.opts, _ = parseArgs(app.args)
	fmt.Println(app.opts)
	fmt.Println(pkg.Add(123, -56))
	return nil
}

func parseArgs(rawArgs []string) (map[string]interface{}, error) {
	version := fmt.Sprintf(
		"%s %s %s",
		Version,
		RepoBranch,
		BuildDate,
	)
	binary := path.Base(rawArgs[0])
	usage := fmt.Sprintf(usageTemplate, binary)
	pargs, err := docopt.ParseArgs(usage, rawArgs[1:], version)
	//fmt.Print(rawArgs, pargs); os.Exit(0)
	return pargs, err
}
