package main

import (
	"fmt"
	"testing"

	assert "github.com/stretchr/testify/assert"
)

func Test_renderUsageTemplate(t *testing.T) {
	exp := fmt.Sprintf(usageTemplate, "abc.exe")

	assert.Equal(t, exp, renderUsageTemplate("/jsdf/ksdjf/abc.exe"))
	assert.Equal(t, exp, renderUsageTemplate("C:\\lsdfj\\ksdfn\\abc.exe"))

	exp = fmt.Sprintf(usageTemplate, "abc")
	assert.Equal(t, exp, renderUsageTemplate("/jsdf/ksdjf/abc"))
	assert.Equal(t, exp, renderUsageTemplate("C:\\lsdfj\\ksdfn\\abc"))
}
