package pkg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAdd(t *testing.T) {
	assert.Equal(t, 5, Add(2,3))
	assert.Equal(t, 3, Add(8,-5))
}